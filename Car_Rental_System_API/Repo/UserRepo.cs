﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Car_Rental_System_API.Models;
using System.Threading.Tasks;

namespace Car_Rental_System_API.Repo
{
    public class UserRepo : IDisposable
    {
        private AuthContext authContext;
        private UserManager<IdentityUser> userManager;

        public UserRepo()
        {
            authContext = new AuthContext();
            userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(authContext));
            userManager.UserValidator = new UserValidator<IdentityUser>(userManager)
            {
                AllowOnlyAlphanumericUserNames = false
            };
        }

        public IdentityResult Register(RegisterModel registerModel)
        {
            IdentityUser newUser = new IdentityUser
            {
                UserName = registerModel.UserName
            };

            return userManager.Create(newUser, registerModel.Password);
        }
        public IdentityUser Login(string userName, string password)
        {
            IdentityUser loginUser = userManager.Find(userName, password);
            return loginUser;
        }

        public async Task<IdentityUser> ChangePassword(IdentityUser user, string newPassword, UserStore<IdentityUser> store, UserManager<IdentityUser> userManager)
        {
            string hashedPassword = userManager.PasswordHasher.HashPassword(newPassword);
            try
            {
                await store.SetPasswordHashAsync(user, hashedPassword);
            }
            catch (Exception err)
            {
                throw err;
            }

            return user;
        }

        public void Dispose()
        {
            authContext.Dispose();
            userManager.Dispose();
        }
    }
}