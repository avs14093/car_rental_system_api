﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Car_Rental_System_API.Models
{
    public class NewBookingModel
    {
        [Required]
        public int carstock_id { get; set; }

        [Required]
        public DateTime from { get; set; }

        [Required]
        public DateTime to { get; set; }
    }
}