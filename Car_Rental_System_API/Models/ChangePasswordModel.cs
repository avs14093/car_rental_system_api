﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Car_Rental_System_API.Models
{
    public class ChangePasswordModel
    {

        [Required]
        public String newPassword { get; set; }
    }
}