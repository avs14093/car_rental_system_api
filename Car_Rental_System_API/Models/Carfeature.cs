//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Car_Rental_System_API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Carfeature
    {
        public Carfeature()
        {
            this.Car_Has_Features = new HashSet<Car_Has_Features>();
        }
    
        public int CarfeatureId { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<Car_Has_Features> Car_Has_Features { get; set; }
    }
}
