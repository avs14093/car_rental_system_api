﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Car_Rental_System_API.Models;
using Car_Rental_System_API;
using Car_Rental_System_API.Repo;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;

namespace Car_Rental_System_API.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private UserRepo userRepo = new UserRepo();
        private CarEntities db = new CarEntities();

        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public IHttpActionResult Register(RegisterModel registerModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = userRepo.Register(registerModel);

            if (result.Succeeded)
            {
                return Ok();
            }
            return BadRequest("Registration failed.");
        }

        [Authorize]
        [Route("changeData")]
        [HttpPut]
        public IHttpActionResult ChangeData(RegisterModel registerModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            String authUserName = User.Identity.Name;
            String authUserId = db.AspNetUsers.First(u => u.UserName == authUserName).Id;

            AspNetUser user = db.AspNetUsers.Find(authUserId);
            user.UserName = registerModel.UserName;

            db.SaveChanges();

            return Ok();
        }

        [Authorize]
        [Route("changePassword")]
        [HttpPut]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordModel changePwModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityUser user = null;

            AuthContext _ctx = new AuthContext();
            UserStore<IdentityUser> userStore = new UserStore<IdentityUser>(_ctx);
            UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(userStore);

            using (UserRepo _repo = new UserRepo())
            {
                user = await userManager.FindByNameAsync(User.Identity.Name);

                user = await _repo.ChangePassword(user, changePwModel.newPassword, userStore, userManager);
                try
                {
                    userStore.AutoSaveChanges = true;
                }
                catch (Exception ex) { throw ex; }
            }
            return Ok();
        }
    }
}