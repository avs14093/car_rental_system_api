﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Car_Rental_System_API.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Car_Rental_System_API.Controllers
{
    [RoutePrefix("api/bookings")]
    [Authorize]
    public class BookingController : ApiController
    {
        private CarEntities db = new CarEntities();

        [HttpPost]
        [Route("")]
        public IHttpActionResult NewBooking(NewBookingModel bookingModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Booking newBooking = new Booking();
            String authUserName = User.Identity.Name;

            newBooking.UserId = db.AspNetUsers.First(u => u.UserName == authUserName).Id;
            newBooking.FromDate = bookingModel.from;
            newBooking.ToDate = bookingModel.to;
            newBooking.CarstockId = bookingModel.carstock_id;

            db.Bookings.Add(newBooking);
            db.SaveChanges();

            return Ok();
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetBookings()
        {
            String authUserName = User.Identity.Name;
            String authUserId = db.AspNetUsers.First(u => u.UserName == authUserName).Id;
            var bookings = (from b in db.Bookings
                       where b.UserId == authUserId
                       select new
                       {
                           id = b.BookingId,
                           user_id = b.UserId,
                           fromDate = b.FromDate,
                           toDate = b.ToDate,
                           carstock_id = b.CarstockId
                       });

            return Ok(bookings);
        }
    }
}