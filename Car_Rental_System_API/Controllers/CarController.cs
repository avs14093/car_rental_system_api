﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Car_Rental_System_API.Models;

namespace Car_Rental_System_API.Controllers
{
    [RoutePrefix("api/availableCars")]
    [Authorize]
    public class CarController : ApiController
    {
        private CarEntities db = new CarEntities();

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetCarModels(string pickupTime)
        {
            DateTime pickupDateTime;
            try
            {
                pickupDateTime = DateTime.Parse(pickupTime);
            }
            catch (FormatException ex)
            {
                return BadRequest("Invalid url param pickupTime format");
            }

            var availableCarstocks = (from s in db.Cars
                       join sa in db.Carstocks on s.CarId equals sa.CarId
                       where !db.Bookings.Any(nextBooking => nextBooking.CarstockId == sa.CarstockId && (nextBooking.FromDate <= pickupDateTime && nextBooking.ToDate >= pickupDateTime))
                       select new
                       {
                           carstockId = sa.CarstockId,
                           id = s.CarId,
                           name = s.Name,
                           doors = s.Doors,
                           seats = s.Seats,
                           basePrice = s.basePrice
                       });

            return Ok(availableCarstocks);
        }

        [HttpGet]
        [Route("{carstockId}")]
        public IHttpActionResult GetCarModel(int carstockId, string pickupTime)
        {
            DateTime pickupDateTime;
            try
            {
                pickupDateTime = DateTime.Parse(pickupTime);
            }
            catch (FormatException ex)
            {
                return BadRequest("Invalid url param pickupTime format");
            }

            var bla = (from s in db.Cars
                       join sa in db.Carstocks on s.CarId equals sa.CarId
                       where !db.Bookings.Any(nextBooking => nextBooking.CarstockId == sa.CarstockId && (nextBooking.FromDate <= pickupDateTime && nextBooking.ToDate >= pickupDateTime))
                       && sa.CarstockId == carstockId
                       select new
                       {
                           carstockId = sa.CarstockId,
                           id = s.CarId,
                           name = s.Name,
                           doors = s.Doors,
                           seats = s.Seats,
                           basePrice = s.basePrice
                       });

            return Ok(bla.First());
        }
    }
}